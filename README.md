# Automated_Juice_Dispenser_UT

Repository for final project of Robotics course. 
Control of a juice dispenser

## Team Members:

* Fabian Parra
* Muhammad Usman
* Anželka Bidzilja
* Reelika Jõuram



## Project Overview:
				
This document describes the final project of robotics course. The main objective of the project is to create an automatic
juice dispenser that fill a glass to a specific value given by the user, between 50 - 187 ml, within a time limit of 30 
seconds. The requested amount of liquid can be sent to the robot through user interface.

For the prove of concept, a peristaltic pump is controller with a PID stage that modifies the motor speed, and the feedback
is made with an encoder or hall effect sensor, that helps to determine the amount of liquid provided. It might be implemented
a level sensor to determine the amount of liquid in main tank which can be necessary to proceed ahead, otherwise the task 
cannot be completed. Further, in order to detect that there is a glass located in the platform, an ultrasonic sensor is used.
For user interface we will use an LCD 16x2 in order to display some measurements, inputs and outputs of the system. Finally,
to design and implement automatic juice dispenser that’s fill the given amount of liquid very precisely and accurately, we
will integrate all controllers, sensors and other components together to accomplish objective of project.  



## Project Schedule 

| Week  |Task  |  Hours
| |-------------|------------- | -------------
| 1  |Benchmarking| 10
| 1  |Design| 10
| 1-2  |Build the structure| 20 *
| 2  |Programming| 30
| 2  |Testing - Tunning the control (Calibration)| 20
| 3  |Communication protocol | 10
| 3  |Create the user interface | 15
| 4  |Integration | 15
| 4  |Demo Video | 5
| 4  |Report - Poster	| 5
| 4  |Solve unforseens	| 5
| 5  |Presentation	| -

*Time may vary depending on the components

## Component list

|    | Element                          | Provide by us       | Provide by Instructors | Total |
|----|----------------------------------|--------------|-------------|-------|
| 1  | [Peristaltic Pump](https://www.boxerpumps.com/en/products/peristaltic-pumps-liquid/boxer-9qq-miniature-peristaltic-pump-series/#c1652)                |              | 1           | 1     |
| 2  | [Arduno nano](https://store.arduino.cc/arduino-nano)                      |              | 1           | 1     |
| 3  | [Raspberry Pi](https://www.raspberrypi.org/products/)                     |              | 1           | 1     |
| 4  | [Battery 12 V](https://www.ebay.com/itm/Turnigy-3S-2200mAh-Lipo-Battery-Pack-11-1v-12v-20-30C-3-cell-DJI-Phantom-FPV-USA-/281246158170)                     |              | 1           | 1     |
| 5  | [Motor   Driver  L293D](http://www.ti.com/product/L293D) or similar |              | 1           | 1     |
| 6  | [Mosfet](https://www.theengineeringprojects.com/2017/06/introduction-to-irf540.html)                           |              | 1           | 1     |
| 7  | [LCD multibutton I2C 16x2](https://www.adafruit.com/product/1110)         |              | 1           | 1     |
| 8  | [Power switch](https://www.alibaba.com/product-detail/Smart-Electronics-Small-ship-type-switch_60284713608.html)                      |              | 1           | 1     |
| 9  | Connecting Wires                 |              | 10 pairs    | 20    |
| 10 | [Breadboard](https://store.digilentinc.com/solderless-breadboard-kit-small-solderless-breadboard-with-two-power-rails/)                       |              | 1           | 1     |
| 11 | [Leds](https://solarbotics.com/product/sled/)                             |              | 2           | 2     |
| 12 | Resistors                        |              | 5           | 5     |
| 13 | [Ultrasonic Sensor](https://components101.com/ultrasonic-sensor-working-pinout-datasheet)                |              | 1           | 1     |
| 14 | [Magnetic Hall effect sensor](https://arduinomodules.info/ky-024-linear-magnetic-hall-module/) or similar      |              | 1           | 1     |
| 15 | Food Grade Silicone Tubing       |              | 2           | 2     |
| 16 | [Rotary encoder module](https://www.openimpulse.com/blog/products-page/product-category/rotary-encoder-module-arduino/)            |              | 1           | 1     |
| 17 | Potentiometer                    |              | 1           | 1     |
| 18 | [Voltage Regulator 3.3v](https://www.adafruit.com/product/2165)           |              | 1           | 1     |
| 19 | Tank                             | 1            |             | 1     |
| 20 | Glass                            | 1            |             | 1     |
| 21 | Structure                        | 1            |             | 1     |
| 22 | Multimeter                       |              | 1           | 1     |




## Challenges and Solutions

Our first attempt is to build a peristaltic pump. At this time, the mechanism is working, but it is necessary to assemble the parts correctly (free movement) and select the correct silicon pipe for best results.
One of our concerns is the limitation of the actuator itself, to pump the desired amount of liquid in a given time.

For the interface, we are using a LCD screen, but for now it only has commands with the up and down buttons, which increase or decrease the engine speed, and the select button to return to 0.
It must be updated with some stages such as: Wait for command, pump juice, finish, etc.
It would also have the measurement provided by the ultrasonic sensor. 

We have measuring part, which works on Arduino Uno with ultrasonic sensor. It can calculate the amount of liquid in mug. But now it's not correct, it has error by ~10ml. 


## Main task

* Muhammad Usman: 
Control the DC Motor - LCD  - programming
* Fabian Parra: 
Structure for peristaltic pump. CAD Model - interface
* Anželka Bidzilja: 
Doing the measure - Arduino Uno and ultrasonic - programming
* Reelika Jõuram: 
Doing the measure - Arduino Uno and ultrasonic - programming



#include <Wire.h>
#include <Adafruit_RGBLCDShield.h>
#include <utility/Adafruit_MCP23017.h>

Adafruit_RGBLCDShield lcd = Adafruit_RGBLCDShield();

#define RED 0x1
#define YELLOW 0x3
#define GREEN 0x2
#define TEAL 0x6
#define BLUE 0x4
#define VIOLET 0x5
#define WHITE 0x7

int motor_pin = 9;
int dir0 = 7;
int dir1 = 8;
int motor_speed =0;
bool select = false;
int stage = 0;
int setpoint = 30;

int echoPin = 2;
int trigPin = 3;
int delay_us = 10;
long distance_mm = 0;
long duration_us;
float korgus = 0;
float sensori_kruusivahe = 8.4;
float ruumala = 0;
float kruusi_korgus = 85; 
float kruusi_ruumala = 0;
int vee_ruumala = 0;

int filtri_suurus = 20; // for filter we use 100 elements
int kokku[20]={};
int summa;
int vee_ruumala_avg;
int indeks = 0;


float total_x =102;
float h_liquid = 0;
//float area = 43.00; //ruutsentimeetrit

float area = 3805; //mm

float pohja_pindala=0;

int count = 0;

void setup() {
  // Debugging output
  Serial.begin(9600);
  // set up the LCD's number of columns and rows: 
  lcd.begin(16, 2);

  pinMode(motor_pin, OUTPUT);
  pinMode(dir0, OUTPUT);
  pinMode(dir1, OUTPUT);

  pinMode(echoPin, INPUT);
  pinMode(trigPin, OUTPUT);
  Serial.println("starting");

  int time = millis();
  lcd.print("Juice Dispenser");
  lcd.setCursor(0,1);
  lcd.print("Robotics Project");
  delay(2000);  
  lcd.clear();
  stage = 1;
  //time = millis() - time;
  //Serial.print("Took "); Serial.print(time); Serial.println(" ms");
  lcd.setBacklight(WHITE);
}

uint8_t i=0;
void loop() {
  // set the cursor to column 0, line 1
  // (note: line 1 is the second row, since counting begins with 0):
  //lcd.setCursor(0, 1);
  // print the number of seconds since reset:
  //lcd.print(millis()/1000);
  
  
  uint8_t buttons = lcd.readButtons();

  if (stage ==1){
    lcd.setBacklight(GREEN);
    //lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("Volume: ");
    lcd.print(setpoint);
    lcd.setCursor(11,0);
    lcd.print(" ml");

       if (buttons) {
      //lcd.clear();
      //lcd.setCursor(0,0);
      if (buttons & BUTTON_UP) {
        //motor_speed = motor_speed + 5;
        setpoint = setpoint + 1;
        //lcd.print("SPEED UP ");
        //lcd.setBacklight(RED);
         if (setpoint>200) {  // ml
          setpoint=200;
          //lcd.clear();        
          }
          //lcd.clear();
      }
      else if (buttons & BUTTON_DOWN) {
        //motor_speed = motor_speed - 5;
        //lcd.print("SPEED DOWN ");
        //lcd.setBacklight(YELLOW);
        setpoint = setpoint - 1;
        if (setpoint<0) {
          setpoint=0;
          }       
      }
      else if (buttons & BUTTON_SELECT) {
      select = true;
      stage = 2;     
      vee_ruumala = 0;
      
      lcd.setBacklight(RED);            
      }
      lcd.clear();
    }
  }
  else if (stage ==2){
      digitalWrite(trigPin, HIGH);
      delayMicroseconds(delay_us);
      digitalWrite(trigPin, LOW);
      
      // Read the pulse HIGH state on echo pin 
      // the length of the pulse in microseconds
      duration_us = pulseIn(echoPin, HIGH);
      distance_mm = duration_us * 0.34/2;

      //korgus = distance_mm - sensori_kruusivahe;                      //height
      vee_ruumala = round(((total_x - distance_mm)* area)/1000);      //water_volume

      Serial.println("Total");
      Serial.println(total_x);
      Serial.println("distance");
      Serial.println(distance_mm);
      Serial.println("water");
      Serial.println(vee_ruumala);
      if (vee_ruumala > 0 && vee_ruumala < 250){
        summa = 0;
      
        kokku[indeks] = {vee_ruumala};
        indeks = (indeks + 1)%filtri_suurus;
      
        for(int i = 0; i < filtri_suurus; i = i +1) {
          summa += kokku[i];

        }
        vee_ruumala_avg = summa/filtri_suurus;
        Serial.println("Average");
        Serial.println(vee_ruumala_avg);
      }
      
      //lcd.clear();
      lcd.setCursor(0,0);
      lcd.print("Volume: ");
      lcd.print(setpoint);
      lcd.print(" ml");  
      lcd.setCursor(0,1);

      if (vee_ruumala_avg<0){
        vee_ruumala_avg=-vee_ruumala_avg;
        
        //lcd.print("-");
        }
      int a= int(vee_ruumala_avg/100);
      int b= int((vee_ruumala_avg-a*100)/10);
      int c= vee_ruumala_avg%10;
      //lcd.print(vee_ruumala);
      lcd.print(a);
      lcd.print(b);
      lcd.print(c);
      lcd.setCursor(11,1);
      lcd.print(" ml");    
    }   

  if (select== true){
     //motor_speed = 0;
     //lcd.clear();
     //lcd.setCursor(0,0);
      //lcd.print("STOP ");
      //lcd.setBacklight(VIOLET);
      motor_speed = 255;

      //motor limit   
     if (motor_speed >= 255){
      motor_speed=255;
      }
      else if (motor_speed <= -255){
        motor_speed=-255;
        }
  
      if (motor_speed < 0) {
            //motor_speed = -motor_speed;        
            digitalWrite(dir1, LOW);
            digitalWrite(dir0, HIGH);        
            analogWrite(motor_pin, -motor_speed);
        } else if (motor_speed > 0) {               
            digitalWrite(dir0, LOW);
            digitalWrite(dir1, HIGH);
            analogWrite(motor_pin, motor_speed);
        }
        else {
          digitalWrite(dir1, LOW);
            digitalWrite(dir0, LOW);
          }

      count ++;

      if (vee_ruumala_avg > setpoint){
        motor_speed = 0;
        lcd.clear();
        lcd.print("Finish");        
        delay(3000);
        lcd.clear();
        delay(20);
        setpoint =0;
        stage=1;
        select=false;
        count =0;
        digitalWrite(dir1, LOW);
        digitalWrite(dir0, LOW);
        analogWrite(motor_pin, motor_speed);
      }
      
      if (motor_speed > 0){
        motor_speed = motor_speed -5;
        
        //lcd.setCursor(0,1);
        //lcd.print(motor_speed);  
        }
        else if (motor_speed < 0){
        motor_speed = motor_speed +5;
          
        //lcd.setCursor(0,1);
        //lcd.print(motor_speed); 
        } 
        
        else{
          select = false;
        }       
        delay(100);
    }
   
    //delay(1);    
}

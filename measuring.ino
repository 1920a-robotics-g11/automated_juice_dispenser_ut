// Datasheet for Ultrasonic Ranging Module HC - SR04
// https://cdn.sparkfun.com/datasheets/Sensors/Proximity/HCSR04.pdf

int echoPin = 10;
int trigPin = 9;
int delay_us = 0;
long distance_cm = 0;
long duration_us;
float korgus = 0;
float sensori_kruusivahe = 7.8;
float ruumala = 0;
float kruusi_korgus = 7.8;
float kruusi_ruumala = 0;
float vee_ruumala = 0;

void setup()  {
  Serial.begin(9600);
  // YOUR SETUP CODE GOES HERE
  pinMode(echoPin, INPUT);
  pinMode(trigPin, OUTPUT);
  // In this section you should initialize serial connection to Arduino
  // and set echoPin and trigPin to correct modes
  Serial.println("starting");
}

void loop() {
  // To generate the ultrasound we need to
  // set the trigPin to HIGH state for correct ammount of µs.
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(delay_us);
  digitalWrite(trigPin, LOW);
  
  // Read the pulse HIGH state on echo pin 
  // the length of the pulse in microseconds
  duration_us = pulseIn(echoPin, HIGH);
  distance_cm = duration_us * 0.034/2;
  korgus = distance_cm - sensori_kruusivahe;
  float pohja_pindala = 43.00; //ruutsentimeetrit
  kruusi_ruumala = kruusi_korgus * pohja_pindala;
  ruumala = korgus * pohja_pindala;
  vee_ruumala = kruusi_ruumala - ruumala;
  Serial.print("Max volume ");
  Serial.println(kruusi_ruumala);
  Serial.print("distance between sensor and bottom of the mug ");
  Serial.println(distance_cm);
  Serial.print("distance between liquid and corner of the mug ");
  Serial.println(korgus);
  Serial.print("empty volume = ");
  Serial.println(ruumala);
  if (vee_ruumala > 0) {
    Serial.print("water volume ");
    Serial.println(vee_ruumala);
  }
  delay(1000);
}
